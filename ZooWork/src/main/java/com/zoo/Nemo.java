package com.zoo;

public class Nemo {
    public static void getInfo() {
        System.out.println("-*-*-*-");
        System.out.println("You have bought a ticket to " +  Nemo.class.getSimpleName());
        System.out.println("His cage is situated at the east side of Zoo");
    }
}

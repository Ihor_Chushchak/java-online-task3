package com.zoo;
import java.util.Scanner;

public class Entrance {
    static Entrance ticket;

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        System.out.println("What ticket you want to buy(if you want to look at Fishes - press f, if you want to look at Birds - press b)?");
        String Information = scanner.next();
        switch (Information) {
            case "f":
                Nemo.getInfo();
                break;
            case "b":
                Parrot.getInfo();
                System.out.println("He is " + Parrot.getEat("Vegeterian"));
                Hawc.getInfo();
                System.out.println("He is " + Hawc.getEat("Non vegeterian"));
                break;
            default :
                System.out.println("Wrong data!");
                System.exit(0);

        }
        System.out.println("Have a nice day!");
    }
}

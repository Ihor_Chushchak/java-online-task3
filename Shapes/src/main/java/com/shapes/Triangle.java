package com.shapes;
public class Triangle extends Shapes {
    public static double getPerimeter(int a, int b, int c) {
        return a + b + c;
    }
    public static double getSquare(int a, int b, int c) {
        double p = getPerimeter(a, b, c)/2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }
    public static void run(int a,int b,int c) {
        System.out.println("\nYour shape is " + Triangle.class.getSimpleName());
        System.out.println("Perimetr = " + Triangle.getPerimeter(a,b,c));
        System.out.println("Square = " + Triangle.getSquare(a,b,c));
        if(a==b||b==c||c==a){
            System.out.println("Congratulations, your triangle is isoscele indeed!");
        }
        if(a==b&&b==c&&c==a){
            System.out.println("Congratulations, your triangle is equilateral indeed!");
        }
        else if(a!=b&&b!=c&&c!=a){System.out.println("Congratulations, your triangle is  scalene indeed!");
        }
        return;
    }}
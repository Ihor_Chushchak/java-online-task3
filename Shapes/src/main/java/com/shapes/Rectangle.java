package com.shapes;

public class Rectangle extends Shapes {

    public static double getSquare(int a, int b) {
        return a * b;
    }

    public static double getPerimeter(int a, int b) {
        return (a+b)*2;
    }
    public static boolean shapeSquare(int a, int b){
        if(a==b){
            return true;
        }
        return false;
    }
    public static void run(int a,int b) {
        System.out.println("\nYour shape is " + Rectangle.class.getSimpleName());
        System.out.println("Perimetr = " + Rectangle.getPerimeter(a,b));
        System.out.println("Square = " + Rectangle.getSquare(a,b));
        if(shapeSquare(a,b)==true){
            System.out.println("Congratulations, your rectangle is square indeed!");
        }
        return;
    }
}

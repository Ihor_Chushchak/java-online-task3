package com.shapes;
import java.util.*;
public class Shapes {

    private static Rectangle initRectangle(){
        System.out.print("Enter 2 sides of rectangle: ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        Rectangle.run(a,b);
        return null;
    }

    private static Triangle initTriangle(){
        System.out.print("Enter 3 sides of triangle: ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        Triangle.run(a,b,c);
        return null;
    }

    private static Circle initCircle(){
        System.out.print("Enter radius of circle: ");
        int r = scanner.nextInt();
        return null;
    }
    private static Shapes shape;
    private static Scanner scanner = new Scanner(System.in);;
    public static void main(String[] args) {

            System.out.println("Choose shape you want to work with(1 - is Rectangle, 2 - is Triangle, 3 - is Circle):");
            switch (scanner.nextInt()) {
                case 1:
                    shape = initRectangle();
                    break;
                case 2:
                    shape = initTriangle();
                    break;
                case 3:
                    shape = initCircle();
                    break;
                default :
                    System.out.println("Wrong data!");
                    System.exit(0);

            }
        }
}





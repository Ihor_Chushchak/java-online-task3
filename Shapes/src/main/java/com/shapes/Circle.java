package com.shapes;

public class Circle extends Shapes {
    public static double getPerimeter(int r) {
        return 2 * Math.PI * r;
    }
    public static double getSquare(int r) {
        return Math.PI * r * r;
    }
    public static void run(int r) {
        System.out.println("\nYour shape is " + Circle.class.getSimpleName());
        System.out.println("Perimetr = " + Circle.getPerimeter(r));
        System.out.println("Square = " + Circle.getSquare(r));

        return;
    }
}